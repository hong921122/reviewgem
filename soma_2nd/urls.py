from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from django.conf import settings

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'soma_2nd.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^index/', 'soma_2nd.views.home'),
    url(r'^detail/(?P<product_id>\d+)', 'soma_2nd.views.detail', name='detail'),
    url(r'^category/(?P<category>\d+)', 'soma_2nd.views.category'),
    url(r'^review_list/', 'soma_2nd.views.review_list'),
    url(r'^twit_list/', 'soma_2nd.views.twit_list'),
    url(r'^twit_submit/', 'soma_2nd.views.twit_content_submit'),
    url(r'^like/', 'soma_2nd.views.like'),
    url(r'^search/', 'soma_2nd.views.search', name='search'),
    url(r'^twit_update/', 'soma_2nd.views.twit_update'),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT, 'show_indexes': settings.DEBUG}),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()
