#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.template import Context, loader
from django.http import Http404
from django.http import HttpResponse, HttpResponseRedirect
from soma_2nd.models import Product, Category, Review, Twit, Pros, Cons, Product_Like, Review_Like
from django.utils import simplejson
from django.contrib.auth.models import * 
from django.contrib.auth import *
from django.contrib.auth import login as auth_login
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import MultipleObjectsReturned
from django.contrib.auth.decorators import login_required
from soma_2nd.review_filter import *


def home(request):
    request.encoding = 'utf-8'

    phone_category = Category.objects.filter(category_code1=10).values('id', 'category_name2')
    phone_lists = Product.objects.filter(category_id=Category.objects.filter(category_code1=10)).values('id', 'name', 'image_url')

    camera_category = Category.objects.filter(category_code1=11)
    camera_lists = Product.objects.filter(category_id=camera_category).values('id', 'name', 'image_url')

    notebook_category = Category.objects.filter(category_code1=12)
    notebook_lists = Product.objects.filter(category_id=notebook_category).values('id', 'name', 'image_url')

    phone_reviews = [{'name' : 'Galaxy S4', 
                      'img' : 'http://timg.danawa.com/prod_img/500000/870/115/img/2115870_1.jpg',
                      'reviews' : [{'title' : '갤4 문제있어요..;'},
                                   {'title' : '갤럭시 s4 사용 후기'},
                                   {'title' : '갤럭시 S4 스펙 및 특징점'}]},
                     {'name' : 'iPhone 5S', 
                      'img' : 'http://timg.danawa.com/prod_img/500000/961/268/img/2268961_1.jpg',
                      'reviews' : [{'title' : '아이폰5 장단점'},
                                   {'title' : 'iphone 5S'},
                                   {'title' : '디자인의 혁신 - iPhone 5S'}]},
                     {'name' : 'Nexus 5',
                      'img' : 'http://timg.danawa.com/prod_img/500000/553/317/img/2317553_1.jpg',
                      'reviews' : [{'title' : '구글 레퍼런스 폰 넥서스5'},
                                   {'title' : '넥5를 질렀습니다!!'},
                                   {'title' : '넥서스 5 구매 후기'}]}]
    t = loader.get_template('tests/index.html')
    c = Context({
        'user' : request.user,
        'phone_categories' : phone_category,
        'phone_products': phone_lists,
        'phone_reviews' : phone_reviews,
        'camera_catrgories' : camera_category,
        'camera_products' : camera_lists,
        'notebook_catrgories' : notebook_category,
        'notebook_products' : notebook_lists,
    })
    return HttpResponse(t.render(c))

def test(request):
    request.encoding = 'utf-8'

    phone_category = Category.objects.filter(category_code1=10).values('id', 'category_name2')
    phone_lists = Product.objects.filter(category_id=Category.objects.filter(category_code1=10)).values('id', 'name', 'image_url')

    camera_category = Category.objects.filter(category_code1=11)
    camera_lists = Product.objects.filter(category_id=camera_category).values('id', 'name', 'image_url')

    notebook_category = Category.objects.filter(category_code1=12)
    notebook_lists = Product.objects.filter(category_id=notebook_category).values('id', 'name', 'image_url')

    phone_reviews = [{'name' : 'Galaxy S4', 
                      'img' : 'http://timg.danawa.com/prod_img/500000/870/115/img/2115870_1.jpg',
                      'reviews' : [{'title' : '갤4 문제있어요..;'},
                                   {'title' : '갤럭시 s4 사용 후기'},
                                   {'title' : '갤럭시 S4 스펙 및 특징점'}]},
                     {'name' : 'iPhone 5S', 
                      'img' : 'http://timg.danawa.com/prod_img/500000/961/268/img/2268961_1.jpg',
                      'reviews' : [{'title' : '아이폰5 장단점'},
                                   {'title' : 'iphone 5S'},
                                   {'title' : '디자인의 혁신 - iPhone 5S'}]},
                     {'name' : 'Nexus 5',
                      'img' : 'http://timg.danawa.com/prod_img/500000/553/317/img/2317553_1.jpg',
                      'reviews' : [{'title' : '구글 레퍼런스 폰 넥서스5'},
                                   {'title' : '넥5를 질렀습니다!!'},
                                   {'title' : '넥서스 5 구매 후기'}]}]
    t = loader.get_template('tests/test.html')
    c = Context({
        'user' : request.user,
        'phone_categories' : phone_category,
        'phone_products': phone_lists,
        'phone_reviews' : phone_reviews,
        'camera_catrgories' : camera_category,
        'camera_products' : camera_lists,
        'notebook_catrgories' : notebook_category,
        'notebook_products' : notebook_lists,
    })
    return HttpResponse(t.render(c))



@csrf_exempt
def detail(request, product_id=None):
    request.encoding = 'utf-8'

    try:
        product = Product.objects.get(id = product_id)
        product_detail = product.getProductDetailInfo()

        if (request.method == 'POST') and ('twit_submit' in request.POST):
            twit = Twit(user_id=request.user.id, category_id = product.category_id, product_id = product.id, content = request.POST.get('twit_input'))
            twit.save()
    except Product.DoesNotExist:
        raise Http404
    except Product.MultipleObjectsReturned:
        raise Http404

    t = loader.get_template('tests/detail.html')
    c = Context({
        'user' : request.user,
        'product_detail' : product_detail,
    })
    return HttpResponse(t.render(c))
    

def category(request, category=None):
    request.encoding = 'utf-8'

    try:
        product_list = Product.objects.filter(category_id=category).values('id')
        if not product_list:
            raise Http404
        category_product = []

        for product in product_list:
            category_product.append(Product.objects.get(id=product['id']).getProductInfo())
    except Product.DoesNotExist:
        raise Http404
    except Product.MultipleObjectsReturned:
        raise Http404

    t = loader.get_template('tests/category.html')
    c = Context({
        'user' : request.user,
        'category_product_lists' : category_product,
    })
    return HttpResponse(t.render(c))
    
@csrf_exempt
def login(request):
    request.encoding = 'utf-8'

    try:
        if (request.method == 'POST') and ('login_submit' in request.POST) :
            user = authenticate(username=request.POST.get('user_id'), password=request.POST.get('user_password'))
            if user is not None:
                if user.is_active:
                    auth_login(request, user)
                    return HttpResponseRedirect(request.GET.get('next', settings.LOGIN_REDIRECT_URL))
                else:
                    return HttpResponseRedirect(request.GET.get('next', settings.LOGIN_URL))
            return HttpResponseRedirect(request.GET.get('next', settings.LOGIN_URL)) 

    except User.MultipleObjectsReturned:
        raise Http404

    t = loader.get_template('tests/login.html')
    c = Context({
    })
    return HttpResponse(t.render(c))
    
@csrf_exempt
def account(request):
    request.encoding = 'utf-8'
    if (request.method == 'POST') and ('create_user_account' in request.POST) :
        nickname = request.POST['user_nickname']
        email = request.POST['user_id']
        password = request.POST['user_password']
        password_confirm = request.POST['user_password_confirm']
        if password == password_confirm :
            user = User.objects.create_user(nickname, email, password)
            user.save()
            return HttpResponseRedirect('/login') 

    t = loader.get_template('tests/account.html')
    c = Context({
    })
    return HttpResponse(t.render(c))

def user_logout_view(request):
    logout(request)
    request.session.flush()
    request.user = AnonymousUser
    return HttpResponseRedirect(request.GET.get('next', settings.LOGIN_REDIRECT_URL))

def review_list(request):
    request.encoding = 'utf-8'
    
    product_id = request.GET.get('product_id')
    page = request.GET.get('page')
    filter_value = request.GET.get('filter_value')

    if filter_value == None :
        review_list = Review.objects.filter(product_id = product_id).all()
        review_paginator = Paginator(review_list, 5)
    else :
        review_filter = ReviewFilter(product_id, filter_value)
        review_paginator = Paginator(review_filter.getReviewListAppend(), 5)

    try:
        review_data = review_paginator.page(page)
    except PageNotAnInteger:
        review_data = review_paginator.page(1)
    except EmptyPage:
        review_data = review_paginator.page(review_paginator.num_pages)

    t = loader.get_template('tests/review_list.html')
    c = Context({
        'product_review' : review_data,
    })
    return HttpResponse(t.render(c))
        
    
def twit_list(request):
    request.encoding = 'utf-8'

    product_id = request.GET.get('product_id')
    twit_list = Twit.objects.filter(product_id = product_id).values().order_by('-create_time')

    t = loader.get_template('tests/twit_list.html')
    c = Context({
        'twit_list' : twit_list,
    })
    return HttpResponse(t.render(c))

def custom_404_view(request):
    request.encoding = 'utf-8'
    return render(request, 'tests/404.html')

@login_required
def like(request):
    request.encoding = 'utf-8'

    product_id = request.GET.get('product_id')
    review_id = request.GET.get('review_id')

    if (product_id != None) and (not Product_Like.objects.filter(product_id=product_id, user_id=request.user.id)):
        like = Product_Like(product_id=product_id, user_id=request.user.id)
        like.save()

    if (review_id != None) and (not Review_Like.objects.filter(review_id=review_id, user_id=request.user.id)):
        like = Review_Like(review_id=review_id, user_id=request.user.id)
        like.save()
    
    return HttpResponse(simplejson.dumps(vars), mimetype='test/html') 

@login_required
def twit_content_submit(request):
    request.encoding = 'utf-8'
    if request.method == 'GET' : 
        product_number = request.GET.get('product_id')
        twit = Twit(user_id=request.user.id, category_id = Product.objects.get(pk=product_number).category_id, product_id = product_number, content = request.GET.get('twit_input'))
        twit.save()
        return HttpResponse('success', mimetype='text/html')

    return HttpResponse('fail', mimetype='text/html')
