"""
WSGI config for soma_2nd project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""

import os, sys

path = '~/soma_2nd/soma_2nd'

if path not in sys.path:
   sys.path.append(path)

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "soma_2nd.settings")

import django.core.handlers.wsgi

application = django.core.handlers.wsgi.WSGIHandler()

#from django.core.wsgi import get_wsgi_application
#application = get_wsgi_application()



