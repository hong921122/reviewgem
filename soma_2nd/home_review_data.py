#!/usr/bin/env python
# -*- coding: utf-8 -*-
from soma_2nd.models import Product, Category
import random
import json

originalPath = 'static/review_summary/product/'

def getReviewData():
    reviewData = []
    productData = []
    productList = Product.objects.all().values('id')
    randomProduct = []

    randomIdx = random.sample(xrange(1, len(productList)), 3)
    for i in randomIdx:
        randomProduct.append(productList[i]['id'])

    #randomProduct = random.choice(productList)
    #randomProduct = random.sample(xrange(1, Product.objects.count()+1), 3)
    #Product.objects.filter(category_id=1).count()+1), 3)

    
    for product_id in randomProduct:
        try:
            fileObj = open(originalPath + str(product_id) + '/brief.json')
        except IOError:
            continue
        contents = fileObj.read()
        product = Product.objects.get(pk=product_id)
        reviewList = []
        for review in json.loads(contents):
            reviewList.append({'title' : review['title'], 'url' : review['url']})
        productData.append({'name' : product.name, 'product_id' : product.id,'img' : product.image_url, 'reviews' : reviewList})
        
    return productData

