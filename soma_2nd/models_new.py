#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

import json
import urllib2

class User(models.Model):
    cookie_id = models.TextField()    

class Category(models.Model):
    category_code1 = models.IntegerField()
    category_code2 = models.IntegerField()
    category_code3 = models.IntegerField()
    category_code4 = models.IntegerField()
    category_name1 = models.CharField(max_length=45)
    category_name2 = models.CharField(max_length=45)
    category_name3 = models.CharField(max_length=45)
    category_name4 = models.CharField(max_length=45)

    def __unicode__(self):
        return self.category_name2
        

class Product(models.Model):
    category = models.ForeignKey(Category)
    name = models.CharField(max_length=45)
    product_model = models.CharField(max_length=45)
    image_url = models.TextField()
    like = models.IntegerField(default=0)

    def __unicode__(self):
        return self.name


    def getAppendSentence(self, json_str):
        appendStr = "" 
        for i in json_str:
            appendStr += i['sentence']
            if len(appendStr) > 100:
                break
            else:
                appendStr += " / "

        return appendStr

    def getProductInfo(self):
        try:
            fileObj = open("/root/soma_2nd/static/review_summary/product/" + str(int(self.id)) + "/brief.json")
            jsonStr = json.loads(fileObj.read())
            for i in jsonStr:
                i['sentences'] = self.getAppendSentence(i['sentences'])
        except IOError:
            jsonStr = ""

        return { 'product' : self, 'pros' : self.pros_set.values_list('option1', 'option2', 'option3', 'option4'), 'cons' : self.cons_set.values_list('option1', 'option2', 'option3', 'option4'), 'review' : jsonStr }

    def getProductDetailInfo(self):
        return { 'product' : self, 'pros' : self.pros_set.values_list('option1', 'option2', 'option3', 'option4'), 'cons' : self.cons_set.values_list('option1', 'option2', 'option3', 'option4'), 'twit' : self.twit_set.values().reverse() }

    def getMainReview(self):

        return {'product' : self, 'review' : jsonStr }


class Twit(models.Model):
#    TWIT_FILTER_CHOICES = {
#        '0' : 'Unknown',
#        '8' : 'Battery',
#        '1' : 'Display',
#        '10' : 'Resolution',
#        '3' : 'Camera',
#        '5' : 'Performance',
#        '4' : 'Portability',
#        '9' : 'Price',
#        '2' : 'Design',
#        '7' : 'Grip',
#        '6' : 'Durability'
#    }
    TWIT_FILTER_CHOICES = {
        '0' : '기타',
        '8' : '배터리',
        '1' : '디스플레이',
        '10' : '해상도',
        '3' : '카메라',
        '5' : '자체 성능',
        '4' : '휴대성',
        '9' : '가격',
        '2' : '디자인',
        '7' : '그립감',
        '6' : '내구성'
    }
    user = models.ForeignKey(User)
    category = models.ForeignKey(Category)
    product = models.ForeignKey(Product)
    content = models.TextField()
    username = models.TextField()
    create_time = models.DateTimeField(auto_now_add=True)
    like = models.IntegerField(default=0)
    twit_filter = models.CharField(max_length=2, default='0')#choices=TWIT_FILTER_CHOICES, default='1')

    def __unicode__(self):
        return self.content

    def save(self, *args, **kwargs):
        self.username = str(User.objects.get(id = self.user_id))
        super(Twit, self).save(*args, **kwargs)


class Review(models.Model):
    category = models.ForeignKey(Category)
    product = models.ForeignKey(Product)
    title = models.CharField(max_length=45)
    content = models.TextField()
    summary = models.TextField()
    source_url = models.TextField()
    source = models.CharField(max_length=100)
    hit = models.IntegerField(default=0)
    like = models.IntegerField(default=0)
    create_time = models.DateTimeField(auto_now=True)
    filter_info = models.TextField()


class Alias(models.Model):
    product = models.ForeignKey(Product)
    name = models.CharField(max_length=45)


class Pros(models.Model):
    product = models.ForeignKey(Product)
    option1 = models.TextField()
    option2 = models.TextField()
    option3 = models.TextField()
    option4 = models.TextField()

    def __unicode__(self):
        return self.option1

class Cons(models.Model):
    product = models.ForeignKey(Product)
    option1 = models.TextField()
    option2 = models.TextField()
    option3 = models.TextField()
    option4 = models.TextField()

    def __unicode__(self):
        return self.option1 

class Product_Like(models.Model):
    product = models.ForeignKey(Product)
    user = models.ForeignKey(User)

    def __unicode__(self):
        return self.user

    def save(self, *args, **kwargs):
        if not Product_Like.objects.filter(product_id=self.product_id, user_id=self.user_id) :
            super(Product_Like, self).save(*args, **kwargs)
#            return True
#        else
#            return False

class Review_Like(models.Model):
    review = models.ForeignKey(Review)
    user = models.ForeignKey(User)

    def __unicode__(self):
        return self.user

    def save(self, *args, **kwargs):
        if not Review_Like.objects.filter(review_id=self.review_id, user_id=self.user_id) :
            super(Review_Like, self).save(*args, **kwargs)
#            return True
#        else
#            return False

class TwitLike(models.Model):
    twit = models.ForeignKey(Twit)
    user = models.ForeignKey(User)

    def __unicode__(self):
        return self.user

    def save(self, *args, **kwargs):
        if not TwitLike.objects.filter(twit_id=self.twit_id, user_id=self.user_id):
            super(TwitLike, self).save(*args, **kwargs)


class UserReport(models.Model):
    user = models.ForeignKey(User)
    username = models.CharField(max_length=45)
    error_select = models.CharField(max_length=45)
    error_text = models.TextField()

    def __unicode__(self):
        return self.error_text

    def save(self, *args, **kwargs):
        self.username = str(User.objects.get(id = self.user_id))
        super(UserReport, self).save(*args, **kwargs)


class FacebookSessionError(Exception):
    def __init__(self, error_type, message):
        self.message = message
        self.type = error_type

    def get_message(self):
        return self.message

    def get_type(self):
        return self.type

    def __unicode__(self):
        return u'%s: %s' % (self.type, self.message)


class FacebookSession(models.Model):
    access_token = models.CharField(max_length=255, unique=True)
    expires = models.IntegerField(null=True)
    user = models.ForeignKey(User, null=True)
    uid=models.BigIntegerField(unique=True, null=True)

    class Meta:
        unique_together = (('user', 'uid'), ('access_token', 'expires'))

    def query(self, object_id, connection_type=None, metadata=False):
        import urllib

        url = 'https://graph.facebook.com/%s' % (object_id)
        if connection_type:
            url += '/%s' % (connection_type)

        params = {'access_token' : self.access_token}
        if metadata:
            params['metadata'] = 1

        url += '?' + urllib.urlencode(params)

        response = json.loads(urllib2.urlopen(url).read())

        if 'error' in response:
            error = response['error']
            raise FacebookSessionError(error['type'], error['meesage'])

        return response
