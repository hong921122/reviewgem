#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models


class User(models.Model):
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=60)
    nickname = models.CharField(max_length=60)
    create_time = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.email


class Category(models.Model):
    category_code1 = models.IntegerField()
    category_code2 = models.IntegerField()
    category_code3 = models.IntegerField()
    category_code4 = models.IntegerField()
    category_name1 = models.CharField(max_length=45)
    category_name2 = models.CharField(max_length=45)
    category_name3 = models.CharField(max_length=45)
    category_name4 = models.CharField(max_length=45)

    def __unicode__(self):
        return self.category_name2
        


class Product(models.Model):
    category = models.ForeignKey(Category)
    name = models.CharField(max_length=45)
    product_model = models.CharField(max_length=45)
    image_url = models.TextField()
    like = models.IntegerField(default=0)

    def __unicode__(self):
        return self.name

    def getProductInfo(self):
        return { 'product' : self, 'pros' : self.pros_set.values_list('option1', 'option2', 'option3', 'option4'), 'cons' : self.cons_set.values_list('option1', 'option2', 'option3', 'option4'), 'review' : self.review_set.values_list('title', 'content', 'create_time') }

    def getProductDetailInfo(self):
        return { 'product' : self, 'pros' : self.pros_set.values_list('option1', 'option2', 'option3', 'option4'), 'cons' : self.cons_set.values_list('option1', 'option2', 'option3', 'option4'), 'review' : self.review_set.values_list('title', 'content', 'create_time') , 'twit' : self.twit_set.values_list('content', 'create_time', 'username' ) }
        

class Twit(models.Model):
    user = models.ForeignKey(User)
    category = models.ForeignKey(Category)
    product = models.ForeignKey(Product)
    content = models.TextField()
    create_time = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.content


class Review(models.Model):
    user = models.ForeignKey(User)
    category = models.ForeignKey(Category)
    product = models.ForeignKey(Product)
    title = models.CharField(max_length=45)
    content = models.TextField()
    source = models.CharField(max_length=100)
    hit = models.IntegerField(default=0)
    like = models.IntegerField(default=0)
    create_time = models.DateTimeField(auto_now=True)


class Alias(models.Model):
    product = models.ForeignKey(Product)
    name = models.CharField(max_length=45)


class Pros(models.Model):
    product = models.ForeignKey(Product)
    option1 = models.CharField(max_length=45)
    option2 = models.CharField(max_length=45)
    option3 = models.CharField(max_length=45)
    option4 = models.CharField(max_length=45)

    def __unicode__(self):
        return self.option1 

class Cons(models.Model):
    product = models.ForeignKey(Product)
    option1 = models.CharField(max_length=45)
    option2 = models.CharField(max_length=45)
    option3 = models.CharField(max_length=45)
    option4 = models.CharField(max_length=45)

    def __unicode__(self):
        return self.option1 

class ProductDetail(models.Model):
    inch = models.CharField(max_length=45)
    cpu = models.CharField(max_length=45)
    ram = models.CharField(max_length=45)
    etc = models.TextField()


