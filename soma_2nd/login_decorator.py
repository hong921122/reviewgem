#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib import messages

def my_login_required(function):
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated():
            return function(request.*args, **kwargs)
        else:
            messages.warning(request, '로그인을 해주세요')
            return render_to_response('
    return wrapper
