#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json

class DataKind:
    productId = None
    dataKind = None
    filterVal = None
    
    def __init__(self, product_id, kind=None, filter_value=None):
        self.productId = product_id
        self.dataKind = kind
        self.filterVal = filter_value

    def getFilePath(self):
        if self.dataKind == 'ATTR_ALL':
            filePath = ('%d/attr_all/%s.json' % (int(self.productId), self.filterVal))
        elif self.dataKind == 'ALL_SENTIMENTAL':
            filePath = ('%d/all_all.json') % (int(self.productId))
        elif self.dataKind == 'ALL':
            filePath = ('%d/all_sentimental.json') % (int(self.productId))
        elif self.dataKind == 'SENTIMENTAL_ALL':
            filePath = ('%d/attr/%s_all.json') % (int(self.productId), self.filterVal)
        elif self.dataKind == 'ATTR_SENTIMENTAL':
            filePath = ('%d/attr/%s_sentimental.json' % (int(self.productId), self.filterVal))
        
        return filePath

class ReviewData:
    originDataPath = "/root/soma_2nd/static/review_summary/product/"
    path = None
    kind = None
    filter_value = None
    sentiment_type = None
    fileContents = None
    reviewList = []
    indexObj = None
    sortOrder = None

    def __init__(self, product_id, kind=None, filter_value=None, sentiment_type=None, order=None):
        self.path = DataKind(product_id=product_id, kind=kind, filter_value=filter_value).getFilePath()
        self.filter_value = filter_value
        self.kind = kind
        self.sentiment_type = sentiment_type
        self.sortOrder = order

    def getReviewData(self, page=None):
        try:
            if not self.sortOrder:
                idx = self.getReviewIndex(page)
                fileObj = open(self.originDataPath + self.path)
                fileObj.seek(int(idx[0]))
                jsonStr = json.loads("[" + fileObj.read( int(idx[1]) - int(idx[0]) ) + "]")
            else:
                idx = self.getReviewIndex(page)
                fileObj = open(self.originDataPath + self.path)
                res = ""
                for i in range(0, len(idx)):
                    print idx[int(i)][0]
                    fileObj.seek( int(idx[int(i)][0]) )
                    if i == len(idx)-1:
                        res += fileObj.read( int(idx[i][1]) - int(idx[i][0]) )
                    else:
                        res += (fileObj.read( int(idx[i][1]) - int(idx[i][0]) ) + ',')
                jsonStr = json.loads("[" + res + "]")
        except TypeError:
            return None
        except KeyError:
            return None

        return jsonStr

    def getPageMax(self):
        if len(self.indexObj)%5 == 0:
            return len(self.indexObj)/5
        else:
            return len(self.indexObj)/5+1

    def getReviewIndex(self, page):
        if self.sortOrder:
            indexKey = self.path
        elif self.sortOrder == 'date':
            indexKey = self.path + 'date'

        if self.kind == 'SENTIMENTAL_ALL' or self.kind == 'ALL_SENTIMENTAL':
            if not self.sortOrder:
                indexKey = self.path
            elif self.sortOrder == 'date':
                indexKey = self.path + '/date'
            self.indexObj = json.loads(open('/root/soma_2nd/static/review_summary/product/indices.json').read())[indexKey]
        else:
            if not self.sortOrder:
                indexKey = self.path + '/' + str(self.sentiment_type)
            elif self.sortOrder == 'date':
                indexKey = self.path + '/' + str(self.sentiment_type) + '/date'
            self.indexObj = json.loads(open('/root/soma_2nd/static/review_summary/product/indices.json').read())[indexKey]
        try:
            if not self.sortOrder:
                return [ self.indexObj[ (int(page)-1)*5 ][0], self.indexObj[ ((int(page)-1)*5)+4 ][1] ]
            elif self.sortOrder == 'date':
                return self.indexObj[ (int(page)-1)*5 : ((int(page)-1)*5)+5 ]
        except IndexError:
            if not self.indexObj:
                return None
            elif not self.sortOrder:
                return [ self.indexObj[ (int(page)-1)*5 ][0], self.indexObj[ len(self.indexObj)-1 ][1] ]
            elif self.sortOrder == 'date':
                return self.indexObj[ (int(page)-1)*5 : ((int(page)-1)*5)+5 ]
        
