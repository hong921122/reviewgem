#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

import json
import urllib2

class UserCookie(models.Model):
    cookie_id = models.TextField()
    create_time = models.DateTimeField(auto_now_add=True)

class Category(models.Model):
    category_code1 = models.IntegerField()
    category_code2 = models.IntegerField()
    category_code3 = models.IntegerField()
    category_code4 = models.IntegerField()
    category_name1 = models.CharField(max_length=45)
    category_name2 = models.CharField(max_length=45)
    category_name3 = models.CharField(max_length=45)
    category_name4 = models.CharField(max_length=45)

    def __unicode__(self):
        return self.category_name2
        

class Product(models.Model):
    category = models.ForeignKey(Category)
    name = models.CharField(max_length=45)
    product_model = models.CharField(max_length=45)
    image_url = models.TextField()
    like = models.IntegerField(default=0)

    def __unicode__(self):
        return self.name


    def getAppendSentence(self, json_str):
        appendStr = "" 
        for i in json_str:
            appendStr += i['sentence']
            if len(appendStr) > 100:
                break
            else:
                appendStr += " / "

        return appendStr

    def getProductInfo(self):
        try:
            fileObj = open("/root/soma_2nd/static/review_summary/product/" + str(int(self.id)) + "/brief.json")
            jsonStr = json.loads(fileObj.read())
            for i in jsonStr:
                i['sentences'] = self.getAppendSentence(i['sentences'])
        except IOError:
            jsonStr = ""

        return { 'product' : self, 'pros' : self.pros_set.values_list('option1', 'option2', 'option3', 'option4'), 'cons' : self.cons_set.values_list('option1', 'option2', 'option3', 'option4'), 'review' : jsonStr }

    def getProductDetailInfo(self):
        return { 'product' : self, 'pros' : self.pros_set.values_list('option1', 'option2', 'option3', 'option4'), 'cons' : self.cons_set.values_list('option1', 'option2', 'option3', 'option4'), 'twit' : self.twit_set.values().reverse() }

    def getMainReview(self):
        return {'product' : self, 'review' : jsonStr }


class Twit(models.Model):
    TWIT_FILTER_CHOICES = {
        '0' : '기타',
        '8' : '배터리',
        '1' : '디스플레이',
        '10' : '해상도',
        '3' : '카메라',
        '5' : '자체 성능',
        '4' : '휴대성',
        '9' : '가격',
        '2' : '디자인',
        '7' : '그립감',
        '6' : '내구성'
    }
    user = models.ForeignKey(UserCookie)
    category = models.ForeignKey(Category)
    product = models.ForeignKey(Product)
    content = models.TextField()
    username = models.TextField(default='이름없음')
    create_time = models.DateTimeField(auto_now_add=True)
    like = models.IntegerField(default=0)
    twit_filter = models.CharField(max_length=2, default='0')#choices=TWIT_FILTER_CHOICES, default='1')
    password = models.TextField()

    def __unicode__(self):
        return self.content


class Review(models.Model):
    category = models.ForeignKey(Category)
    product = models.ForeignKey(Product)
    title = models.CharField(max_length=45)
    content = models.TextField()
    summary = models.TextField()
    source_url = models.TextField()
    source = models.CharField(max_length=100)
    hit = models.IntegerField(default=0)
    like = models.IntegerField(default=0)
    create_time = models.DateTimeField(auto_now=True)
    filter_info = models.TextField()


class Alias(models.Model):
    product = models.ForeignKey(Product)
    name = models.CharField(max_length=45)

"""
class Pros(models.Model):
    product = models.ForeignKey(Product)
    content = models.TextField()
    order = models.IntegerField()
    like = models.IntegerField(default=0)
    dislike = models.IntegerField(default=0)

class Cons(models.Model):
    product = models.ForeignKey(Product)
    content = models.TextField()
    order = models.IntegerField()
    like = models.IntegerField(default=0)
    dislike = models.IntegerField(default=0)

class ProsLike(models.Model):
    pros = models.ForeignKey(Pros)
    user = models.ForeignKey(UserCookie)

    def __unicode__(self):
        return self.user

    def save(self, *args, **kwargs):
        if not ProsLike.objects.filter(pros_id=self.pros_id, user_id=self.user_id):
            super(ProsLike, self).save(*args, **kwargs)

class ConsLike(models.Model):
    cons = models.ForeignKey(Cons)
    user = models.ForeignKey(UserCookie)

    def __unicode__(self:
        return self.user

    def save(self, *args, **kwargs):
        if not ConsLike.objects.filter(cons_id=self.cons_id, user_id=self.user_id)
            super(ConsLike, self).save(*args, **kwargs)
"""

class Pros(models.Model):
    product = models.ForeignKey(Product)
    option1 = models.TextField()
    option2 = models.TextField()
    option3 = models.TextField()
    option4 = models.TextField()

    def __unicode__(self):
        return self.option1

class Cons(models.Model):
    product = models.ForeignKey(Product)
    option1 = models.TextField()
    option2 = models.TextField()
    option3 = models.TextField()
    option4 = models.TextField()

    def __unicode__(self):
        return self.option1 


class TwitLike(models.Model):
    twit = models.ForeignKey(Twit)
    user = models.ForeignKey(UserCookie)

    def __unicode__(self):
        return self.user

    def save(self, *args, **kwargs):
        if not TwitLike.objects.filter(twit_id=self.twit_id, user_id=self.user_id):
            super(TwitLike, self).save(*args, **kwargs)

