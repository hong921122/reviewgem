#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json

class DataKind:
    originDataPath = "/root/soma_2nd/static/review_summary/product/"
    filePath = originDataPath
    productId = None
    dataKind = None
    filterVal = None
    sortOrder = 'DEFAULT'
    
    def __init__(self, product_id, kind=None, filter_value=None, sort_order=None):
        self.productId = product_id
        self.dataKind = kind
        self.filterVal = filter_value

    def getFilePath(self):
        if self.dataKind == 'ATTR_ALL':
            self.filePath += ('%d/attr_all/%s.json' % (int(self.productId), self.filterVal))
        elif self.dataKind == 'ALL_SENTIMENTAL':
            self.filePath += ('%d/all_all.json') % (int(self.productId))
        elif self.dataKind == 'ALL':
            self.filePath += ('%d/all_sentimental.json') % (int(self.productId))
        elif self.dataKind == 'SENTIMENTAL_ALL':
            self.filePath += ('%d/attr/%s_all.json') % (int(self.productId), self.filterVal)
        elif self.dataKind == 'ATTR_SENTIMENTAL':
            self.filePath += ('%d/attr/%s_sentimental.json' % (int(self.productId), self.filterVal))
        
        return self.filePath

class ReviewData:
    kind = None
    filter_value = None
    sentiment_type = None
    fileContents = None
    reviewList = []

    def __init__(self, product_id, kind=None, filter_value=None, sentiment_type=None):
        path = DataKind(product_id=product_id, kind=kind, filter_value=filter_value).getFilePath()
        self.filter_value = filter_value
        self.kind = kind
        self.sentiment_type = sentiment_type
        try:
            self.fileContents = open(path).read()
        except IOError:
            self.fileContents = None

    def getReviewData(self, page=None):
        jsonStr = json.loads(self.fileContents)
        self.reviewList = []
        reviewListRes = []
        if jsonStr is None:# jsonStr.get(str(sentiment_type)):
            return None
        if self.kind == 'ATTR_SENTIMENTAL':
            if jsonStr.get(str(self.sentiment_type)) is None:
                return None
            for review in jsonStr.get(str(self.sentiment_type)):
                self.reviewList.append(review)
            for review in self.reviewList[(int(page)-1)*5 : ((int(page)-1)*5)+5]:
                reviewListRes.append(review)
        elif self.kind == 'SENTIMENTAL_ALL' or self.kind == 'ALL_SENTIMENTAL':
            for review in jsonStr:
                self.reviewList.append(review)
            for review in self.reviewList[(int(page)-1)*5 : ((int(page)-1)*5)+5]:
                reviewListRes.append(review)
        elif self.kind == 'ALL':
            if jsonStr.get(str(self.sentiment_type)) is None:
                return None
            for review in jsonStr.get(str(self.sentiment_type)):
                self.reviewList.append(review)
            for review in self.reviewList[(int(page)-1)*5 : ((int(page)-1)*5)+5]:
                reviewListRes.append(review)

        print reviewListRes
        return reviewListRes

    def getPageMax(self):
        if len(self.reviewList)%5 == 0:
            return len(self.reviewList)/5
        else:
            return len(self.reviewList)/5+1
