#!/usr/bin/env python
# -*- coding: utf-8 -*-
from soma_2nd.models import *
import json

originalPath = "/root/soma_2nd/static/review_summary/product/"

class SetFilePath:
    product_name = None
    filter_value = None

    def __init__(self, number=None, value=None):
        self.product_name = Product.objects.get(id=number).id
        self.filter_value = value

    def getAbsoluteFilePath(self):
        if self.filter_value != None and self.filter_value != '11':
            return originalPath + str(self.product_name) + "/attr_sentimental/" + str(self.filter_value) + ".json"
        elif self.filter_value == None or self.filter_value == '11':
            return originalPath + str(self.product_name) + "/all.json"

class ReviewFilter:
    filePath = None
    fileObj = None
    jsonRes = None 
    filter_value = None
    reviewList = []
    
    def __init__(self, number, filter_value=None):
        self.filter_value = filter_value
        self.filePath = SetFilePath(number = number, value = filter_value).getAbsoluteFilePath()
        self.fileObj = open(self.filePath)

    def getReviewFilterList(self):
        self.jsonRes = json.loads(self.fileObj.read())

    def getReviewSentimentList(self, sentiment_type, page):
        self.reviewList = []
        review_list = []
        if self.filter_value != None and self.filter_value != '11':
            tmp = self.jsonRes.get(str(sentiment_type))
            if tmp == None:
                return None
            for review in tmp:
                self.reviewList.append(review)
            for review in self.reviewList[(int(page)-1)*5 : ((int(page)-1)*5)+5]:
                review_list.append(review)
        elif self.filter_value == None or self.filter_value == '11':
            if self.jsonRes == None:
                return None
            for review in self.jsonRes:
                self.reviewList.append(review)
            for review in self.reviewList[(int(page)-1)*5 : ((int(page)-1)*5)+5]:
                review_list.append(review)

        return review_list

    def getPageMax(self):
        if len(self.reviewList)%5 == 0:
            return len(self.reviewList)/5
        else:
            return len(self.reviewList)/5+1
