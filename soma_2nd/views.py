#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.template import Context, loader, RequestContext
from django.http import Http404
from django.http import HttpResponse, HttpResponseRedirect
from soma_2nd.models import *
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect, render, render_to_response
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import MultipleObjectsReturned
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import F
from django.contrib import messages
from django.middleware.gzip import GZipMiddleware
from soma_2nd.home_review_data import *
from soma_2nd.review_data_new import *

import json
import subprocess
import urllib
import cgi
import random
import uuid

gzip_middleware = GZipMiddleware()

def home(request):
    request.encoding = 'utf-8'

    phone_category = Category.objects.filter(category_code1=10).values('id', 'category_name2')
    phone_lists = Product.objects.filter(category_id=Category.objects.filter(category_code1=10)).values('id', 'name', 'image_url')

    camera_category = Category.objects.filter(category_code1=11)
    camera_lists = Product.objects.filter(category_id=camera_category).values('id', 'name', 'image_url')

    notebook_category = Category.objects.filter(category_code1=12)
    notebook_lists = Product.objects.filter(category_id=notebook_category).values('id', 'name', 'image_url')

    product = Product.objects.all().values('id')
    while True:
        try:
            randomNum = random.choice(product)['id']
            randomProduct = Product.objects.get(id=randomNum).getProductDetailInfo()
            chart_data = json.loads(open('static/review_summary/product/' + str(randomNum) + '/origin_attr_count.json').read())
            break
        except IOError:
            continue

    main_review = json.loads(open('static/review_summary/main_reviews.json').read())
    image_review = []
    normal_review = []
    for review in main_review:
        if review['thumb']:
            image_review.append(review)
        else:
            normal_review.append(review)
    review1 = random.sample(image_review, 3)
    for review in review1:
        main_review.remove(review)
    review2 = random.sample(main_review, 7)

    #phone_reviews = getReviewData()
    t = loader.get_template('tests/index.html')
    c = Context({
        'user' : request.user,
        'phone_categories' : phone_category,
        'phone_products': phone_lists,
        #'phone_reviews' : phone_reviews,
        'camera_catrgories' : camera_category,
        'camera_products' : camera_lists,
        'notebook_catrgories' : notebook_category,
        'notebook_products' : notebook_lists,
        'chart_data' : chart_data,
        'random_product' : randomProduct,
        'image_review' : review1,
        'normal_review' : review2,
    })
    response = HttpResponse(t.render(c))

    if not request.COOKIES.has_key('cookie_id'):
        cookie_id = uuid.uuid1()
        response.set_cookie('cookie_id', cookie_id, max_age=3600000000)
        UserCookie(cookie_id=cookie_id).save()

    response['Cache-Control'] = 'no-cache'
    return gzip_middleware.process_response(request, response)


@csrf_exempt
def detail(request, product_id=None):
    request.encoding = 'utf-8'
    cookieFlag = False
    
    if not request.COOKIES.has_key('cookie_id'):
        cookie_id = uuid.uuid1()
        user = UserCookie(cookie_id=cookie_id)
        user.save()
        cookieFlag = True
    else:
        try:
            user = UserCookie.objects.get(cookie_id = request.COOKIES.get('cookie_id'))
        except UserCookie.DoesNotExist:
            cookie_id = uuid.uuid1()
            user = UserCookie(cookie_id=cookie_id)
            user.save()
            cookieFlag = True

    try:
        product = Product.objects.get(id = product_id)
        product_detail = product.getProductDetailInfo()
        branch_data = { 'category_id' : product.category_id, 'category_name1' : product.category.category_name1, 'category_name' : product.category.category_name2, 'product_name' : product.name }
        chart_data = json.loads(open('static/review_summary/product/' + product_id + '/origin_attr_count.json').read())
    except Product.DoesNotExist:
        raise Http404
    except Product.MultipleObjectsReturned:
        raise Http404
    except IOError:
        chart_data = None

    t = loader.get_template('tests/detail.html')
    c = Context({
        'user' : request.user,
        'product_detail' : product_detail,
        'chart_data' : chart_data,
        'branch_data' : branch_data,
    })
    response = HttpResponse(t.render(c))
    if cookieFlag:
        response.set_cookie('cookie_id', user.cookie_id, max_age=3600000000)

    return response
    
def category(request, category=None):
    request.encoding = 'utf-8'

    try:
        product_list = Product.objects.filter(category_id=category).values('id')
        category_number = Category.objects.filter(id=category).values('category_code1')
        category_list = Category.objects.filter(category_code1=category_number).values('category_name2', 'id')
        if not product_list:
            raise Http404
        category_product = []

        for product in product_list:
            category_product.append(Product.objects.get(id=product['id']).getProductInfo())
    except Product.DoesNotExist:
        raise Http404
    except Product.MultipleObjectsReturned:
        raise Http404

    t = loader.get_template('tests/category.html')
    c = Context({
        'user' : request.user,
        'category_product_lists' : category_product,
        'category_list' : category_list,
        'cate_code' : category
    })
    return HttpResponse(t.render(c))

def review_list(request):
    request.encoding = 'utf-8'
    
    product_id = request.GET.get('product_id')
    page = request.GET.get('page')
    filter_value = request.GET.get('filter_value')
    get_sentiment_type = request.GET.get('sentiment_type')
    sort_order = request.GET.get('order')

    if filter_value is None and get_sentiment_type != '3':
        review_filter = ReviewData(product_id=product_id, kind='ALL', sentiment_type=get_sentiment_type, order=sort_order)
        var = review_filter.getReviewData(page)
    elif filter_value is None and get_sentiment_type == '3':
        review_filter = ReviewData(product_id=product_id, kind='ALL_SENTIMENTAL', sentiment_type=get_sentiment_type, order=sort_order)
        var = review_filter.getReviewData(page)
    elif filter_value is not None and get_sentiment_type != '3':
        review_filter = ReviewData(product_id=product_id, filter_value=filter_value, kind='ATTR_SENTIMENTAL', sentiment_type=get_sentiment_type, order=sort_order)
        var = review_filter.getReviewData(page)
    elif filter_value is not None and get_sentiment_type == '3':
        review_filter = ReviewData(product_id=product_id, filter_value=filter_value, kind='SENTIMENTAL_ALL', sentiment_type=get_sentiment_type, order=sort_order)
        var = review_filter.getReviewData(page)

    t = json.dumps({"data" : {"info" : var, "page" : page, "maxpage" : review_filter.getPageMax()}} )

    return HttpResponse(json.dumps(json.loads(t)), content_type = 'text/json')
    
def twit_list(request):
    request.encoding = 'utf-8'

    product_id = request.GET.get('product_id')
    twit_filter = request.GET.get('twit_filter')

    if twit_filter == '11':
        twit_list = Twit.objects.filter(product_id = product_id).values().order_by('-create_time')
    elif twit_filter != None:
        twit_list = Twit.objects.filter(product_id = product_id, twit_filter = twit_filter).values().order_by('-create_time')

    for twit_item in twit_list:
        for key, value in twit_item.iteritems():
            if key == 'twit_filter':
                twit_item[key] = Twit.TWIT_FILTER_CHOICES[value]

    t = loader.get_template('tests/twit_list.html')
    c = Context({
        'twit_list' : twit_list,
    })
    return HttpResponse(t.render(c))

def custom_404_view(request):
    request.encoding = 'utf-8'
    return render(request, 'tests/404.html')

def like(request):
    request.encoding = 'utf-8'
    user = UserCookie.objects.get(cookie_id = request.COOKIES.get('cookie_id'))
    twit_id = request.GET.get('twit_id')
    pros_id = request.GET.get('pros_id')
    cons_id = request.GET.get('cons_id')
    procon_type = request.GET.get('procon_type')

    if (twit_id != None) and (not TwitLike.objects.filter(twit_id=twit_id, user_id=user.id)):
        like = TwitLike(twit_id=twit_id, user_id=user.id)
        like.save()
        twit = Twit.objects.get(id=twit_id)
        twit.like = F('like') + 1
        twit.save()

    return HttpResponse()
"""
    elif (not cons_id) and (not ConsLike.objects.filter(cons_id=cons_id, user_id=user.id)):
        conlike = ConsLike(cons_id=cons_id, user_id=user.id)
        conlike.save()
        cons = Cons.objects.get(pk=cons_id)
        if procon_type == 'dislike':
            cons.dislike = F('dislike') + 1
        elif procon_type == 'like':
            cons.like = F('like') + 1
        cons.save()
    elif (not pros_id) and (not ProsLike.objects.filter(pros_id=pros_id, user_id=user.id)):
        like = ProsLike(pros_id=pros_id, user_id=user.id)
        like.save()
        pros = Pros.objects.get(pk=pros_id)
        if procon_type == 'like':
            pros.like = F('like') + 1
        elif procon_type == 'dislike':
            pros.dislike = F('dislike') + 1
        pros.save()
"""        

def twit_content_submit(request):
    request.encoding = 'utf-8'
    user = UserCookie.objects.get(cookie_id = request.COOKIES.get('cookie_id'))

    if request.method == 'GET' : 
        product_number = request.GET.get('product_id')
        twit = Twit(user_id=user.id, category_id = Product.objects.get(pk=product_number).category_id, product_id = product_number, content = request.GET.get('twit_input'), twit_filter = request.GET.get('twit_tag'), username = request.GET.get('name'), password = request.GET.get('password'))
        twit.save()
        return HttpResponse('success', mimetype='text/html')

    return HttpResponse('fail', mimetype='text/html')

def search(request):
    search_query = request.GET.get('query')
    comm = "java -cp /var/www/nlp/yhannanum/out/production/yhannanum yhannanum/worker/ReviewSearchClient \"%s\"" % (search_query)
    p = subprocess.Popen(comm, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    result = ""
    for line in p.stdout.readlines():
        result += line

    data = json.loads(result)

    """
    sentence_string = ""
    for review in data:
        for sentence in review['sentences']:
            if len(sentence_string) < 500:
                sentence_string += sentence
        review['sentences'] = sentence_string
        sentence_string = ""
    """

    return render_to_response('tests/search.html', { 'data' : json.loads(result) , 'query' : search_query })

def twit_update(request):
    twit_id = request.GET.get('twit_id')
    update_type = request.GET.get('update_type')
    twit = Twit.objects.get(pk=twit_id)

    if (not twit_id) or (not update_type) or (not request.GET.get('password')) or (not request.GET.get('content')):
        return None

    if twit.password == request.GET.get('password'):
        if update_type == "delete":
            twit.delete()
        elif update_type == "edit":
            twit.content = request.GET.get('content')
            twit.save()
        return HttpResponse()
    else:
        return None

def error_report(request):
    request.encoding = 'utf-8'

    if request.method == 'GET' :
        error_text = request.GET.get('error_text')
        error_select = request.GET.get('error_select')
        if error_select != None:
            report = UserReport(error_select = error_select, error_text = error_text, user_id = request.user.id)
        elif error_select == None:
            report = UserReport(error_text = error_text, user_id = request.user.id)
        report.save()

    return HttpResponse()
