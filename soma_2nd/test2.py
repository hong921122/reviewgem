#!/usr/bin/env python
# -*- coding: utf-8 -*-
from soma_2nd.models import *
import json

originalPath = "/var/www/"

class SetFilePath:
    product_name = None
    filter_value = None

    def __init__(self, number=None, value=None):
        self.product_name = Product.objects.get(id = 6).name
        self.filter_value = value

    def getAbsoluteFilePath(self):
        return originalPath + self.product_name + "/attr/" + self.filter_value + ".txt"

class ReviewFilter:
    filePath = None
    fileObj = None
    
    def __init__(self, number, filter_value):
        self.filePath = SetFilePath(input_data = number, value = filter_value).getAbsoluteFilePath()
        self.fileObj = open(self.filePath)

    def getJsonParse(self):
        lineStr = []
        for line in self.fileObj:
            lineStr.append(line)

        return lineStr

    def getReviewFilterList(self):
        tmp = []
        for jsonStr in getJsonParse():
            tmp.append(json.loads(jsonStr))
        return tmp
