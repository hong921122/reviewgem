#!/usr/bin/env python
# -*- coding: utf-8 -*-
from soma_2nd.models import *
from operator import itemgetter, attrgetter
from django.db import models
import json

class ReviewList(Review):
    review_id = None
    rule_number = None
    title = None
    source_url = None
    like = None
    create_time = None

    filter_value = None
    json_string = None
    

    def __init__(self, review_obj, review_id, title, source_url, like, create_time, json_parse_string, filter_value):
        cnt = 0
        for parse in json_parse_string:
            if parse['origin_attr'] == filter_value:
                cnt += 1

        self.rule_number = cnt
        self.review_id = review_id
        self.title = title
        self.source_url = source_url
        self.like = like
        self.create_time = create_time

    def getSummary(self):
        ret_str = ""
        for parse in json_parse_string[filter_value]:
            ret_str += parse['sentence']

        return ret_str

class ReviewFilter:
    attr_info = None
    filter_info = None
    filter_value = None

    def __init__(self, product_id, filter_value):
        self.filter_value = filter_value
        self.filter_info = Review.objects.raw('select * from soma_2nd_review where attr_info like \'%%[%s,%%\' or attr_info like \'%%,%s,%%\' or attr_info like \'%%,%s]\' or attr_info like \'%%[%s]%%\'', [int(filter_value), int(filter_value), int(filter_value), int(filter_value)])
        

#    def getReviewList(self):
#        review_list = []
#        for f_info in self.filter_info:
#            json_parse = json.loads(f_info.filter_info)
#            review_list.append(Review(review_id = f_info.id, title = f_info.title, source_url = f_info.source_url, like = f_info.like, create_time = f_info.create_time, json_parse_string = json_parse['filter_info'], filter_value = self.filter_value) 

        #sorted(review_list, key=itemgetter('rule_number'))
        #return review_list

    def getReviewListAppend(self):
        tmp = []
        for review in self.filter_info:
            tmp.append(review)

        return tmp

    def getReviewFilterInfo(self):
        tmp = []
        for review in self.filter_info:
            tmp.append(review)

        return tmp
