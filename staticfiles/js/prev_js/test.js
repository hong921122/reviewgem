function cateoff(){
    $('#phone_detail').hide();
    $('#camera_detail').hide();
}

function category_init(){
    $('#camera_detail .cate_wrapper').css('border-right','');
    $('#category_camera .cate_wrapper').css('border-right','1px solid #e5e5e5');

    $('#category_phone').hover(function() {
	cateoff();
	$('#phone_detail').show();
	$('#camera_detail .cate_wrapper').css('border-right','');
	$('#category_camera .cate_wrapper').css('border-right','1px solid #e5e5e5');
	$('#category_phone .tab_title').css('color','#7adb30');
	$('#category_phone .tab_title').css('border-bottom',' 3px solid #7abd30');
	$('#category_camera .tab_title').css('color','#838383');
	$('#category_camera .tab_title').css('border-bottom',' 3px solid #ffffff');
    }, function() {});

    $('#category_camera').hover(function() {
	cateoff();
	$('#category_camera .cate_wrapper').css('border-right','');
	$('#camera_detail').show();
	$('#camera_detail .cate_wrapper').css('border-right','1px solid #e5e5e5');
	$('#category_camera .tab_title').css('color','#7adb30');
	$('#category_camera .tab_title').css('border-bottom',' 3px solid #7abd30');
	$('#category_phone .tab_title').css('color','#838383');
	$('#category_phone .tab_title').css('border-bottom',' 3px solid #ffffff');
    }, function() {});
}
function search_init(){
    $("#search input").focus(function() {
	this.value = '';
    });
}
function init(){
    category_init();
    search_init();
}

$(document).ready(init);
