var sentiment_type = 0;
var product_number;

function nav(){
    $('#nav1').click(function(){
	$('html,body').animate({
            scrollTop: $("#product").offset().top - 20},'slow');
    });
    $('#nav2').click(function(){
	$('html,body').animate({
            scrollTop: $("#review").offset().top - 20},'slow');
    });
    $('#nav3').click(function(){
	$('html,body').animate({
            scrollTop: $("#twit").offset().top - 20},'slow');
    });
}

function getHighlight(str){
    var sentences = str['sentences'];
    var complete_sentence = "";
    var highlightElemList = [];
    var sentiment_class = (sentiment_type == 0) ? "sentiment_good" : "sentiment_bad";
    for(var e in sentences){
        var sentence = sentences[e]['sentence'];
        var highlight = sentences[e]['highlighting'];

        if(highlight['sentiment'])
            highlight['sentiment'].forEach(function(elem){highlightElemList.push(["sentiment", elem]);});
        if(highlight['attr'])
            highlight['attr'].forEach(function(elem){highlightElemList.push(["attr", elem]);});

        highlightElemList.sort(function(a,b){
            return (a[1][0] < b[1][0]) ? -1 : 1;
        });

        highlightElemList.forEach(function(elem, index, arr){
            var sub_sentence = "";
            if(elem[0] == "sentiment")
                sub_sentence = "<font class=\"" + sentiment_class + "\">" + sentence.substring(elem[1][0], Number(elem[1][1])+1) + "</font>";
            else
                sub_sentence = "<font class=\"attribute\">" + sentence.substring(elem[1][0], Number(elem[1][1])+1) + "</font>";

            if(index != 0){
                var sub_sentence2 = sentence.substring(arr[index-1][1][1]+1, arr[index][1][0]);
                complete_sentence = complete_sentence + sub_sentence2 + sub_sentence;
            }
            else if(index == 0){
                var sub_sentence3 = sentence.substring(0, arr[index][1][0]);
                complete_sentence = complete_sentence + sub_sentence3  +sub_sentence;
            }
            if(index == arr.length-1){
                complete_sentence = complete_sentence + sentence.substring(arr[index][1][1]+1, sentence.length);
            }
        });

        highlightElemList = [];
        complete_sentence += "<br>";
        if(complete_sentence.length > 500)break;
    }
    
    return complete_sentence;
}

function setReviewData(jsonObj){
    var contents = jsonObj['data']['info'];
    var page = jsonObj['data']['page'];
    var maxpage = jsonObj['data']['maxpage'];
    $('#review_content').html("");
    $('#current_page').html("");
    $('#max_page').html("");
    for(var e in contents){
        var substringTitle = ((new String(contents[e]['title'])).length > 80) ? (new String(contents[e]['title'])).substring(0, 80)+"...." : contents[e]['title'];
console.log(substringTitle);
        var el = $("<div class=\"review\"><div class=\"review_pic\"></div><div class=\"product_review_detail\"><p class=\"review_title\" title=\"" + contents[e]['title'] + "\"><a href=\"" + contents[e]['url'] + "\" target=\"_blank\">" +
                    substringTitle + "</a></p><p class=\"review_date\">" + contents[e]['creation_time'] + "</p><p class=\"review_content\">" + 
                    getHighlight(contents[e]) + "</p></div></div>");
        $('#review_content').append(el);
    }
    $('#current_page').append(page);
    $('#max_page').append(maxpage);
    if(page == 1){
	$('.paging').each(function(){
	    $(this).html(parseInt(this.text)%6);
	    if(parseInt(this.text) > parseInt($('#max_page').html())){
		$(this).hide();
	    }
	    else{
		$(this).show();
	    }
	});
    }
    $('#prev_page').attr('value', Number(jsonObj['data']['page'])-1);
    $('#next_page').attr('value', Number(jsonObj['data']['page'])+1);
}

function getSelectedItem(){
    $("input[name='filter_item']").click(function(){
        $('#procon_tab').show();
        getReviewData(1);
    });
}

function getSentimentData(){
    $('#pros_button').click(function(){
        sentiment_type = 0;
	$('#pros_button').css('color', '#7abd30');
	$('#pros_button').css('border-bottom', '3px solid #7abd30');
	$('#cons_button').css('color', '#c2c2c2');
	$('#cons_button').css('border-bottom', '3px solid #ffffff');
        getReviewData(1);
    });
    $('#cons_button').click(function(){
        sentiment_type = 1;
	$('#pros_button').css('color', '#c2c2c2');
	$('#pros_button').css('border-bottom', '3px solid #ffffff');
	$('#cons_button').css('color', '#7abd30');
	$('#cons_button').css('border-bottom', '3px solid #7abd30');
        getReviewData(1);
    });
}

function getReviewPagination(){
    $('#prev_page').click(function(){
        var page = $('#prev_page').attr('value');
	var page_group = parseInt(parseInt(page)/5) - 1;
        if(Number(page_group + 1) >= 1){
            getReviewData((((page_group) * 5) + 1));
	    $('.paging').each(function(){
		$(this).html(parseInt(this.text) - 5);
		$(this).show();
	    });
        }
    });

    $('#next_page').click(function(){
        var page = $('#next_page').attr('value');
	var page_group = parseInt(parseInt(page)/5) + 1;
        if(Number(((page_group - 1) * 5) + 5) < Number($('#max_page').html())){
            getReviewData(((page_group * 5) + 1));
	    $('.paging').each(function(){
		$(this).html(parseInt(this.text) + 5);
		if(parseInt(this.text) > parseInt($('#max_page').html())){
		    $(this).hide();
		}
	    });
        }
    });
    $('.paging').click(function(){
	getReviewData("" + this.text);
    });
    $('#max_page').click(function(){
	getReviewData("" + this.text);
	var page_group = parseInt((parseInt(this.text)-1) / 5);
	$('.paging').each(function(){
	    var paging_index = (parseInt(this.text) - 1) % 5 + 1;
	    var page_num = page_group * 5;
	    $(this).html(paging_index + page_num);
	    if(parseInt(this.text) > parseInt($('#max_page').html())){
		$(this).hide();
	    }
	});
    });
}


function getReviewData(page){
    var item_val = $("input[name='filter_item']:checked").val();
    var get_data = $.get('http://reviewgem.co.kr/review_list', {filter_value : item_val, page : page, product_id : product_number, sentiment_type : sentiment_type });
    get_data.done(function(data){
        setReviewData(data);
    });
}

function setTwitSubmit(){
    var input_data = $('#twit_input').val();
    $('#twit_submit').mousedown(function(){
        var twit_submit = $.get('http://reviewgem.co.kr/twit_submit', {product_id : product_number, twit_input : $('#twit_input').val()});
    });
}

function getTwitData(event){
    $('#twits').scroll(function(){
        console.log("tes123123123t");
        var elem = $('#twits');
        if( elem[0].scrollHeight - elem.scrollTop() + 7 == elem.outerHeight() ) {
            var twit_data = $.get('http://reviewgem.co.kr/twit_list', {product_id : product_number });
            twit_data.done(function(data){
                $('#twits').html("");
                $('#twits').append(data);
            });
        }
    });
}

window.onload = function(){
    product_number = $('#product_number').val();
    $('#procon_tab').hide();
    var get_data = $.get('http://reviewgem.co.kr/review_list', {page : 1, product_id : product_number});
    get_data.done(function(data){
        setReviewData(data);
    });

/*
    setInterval(function(){
        var twit_data = $.get('http://reviewgem.co.kr/twit_list', {product_id : product_number});
        twit_data.done(function(data){
            $('#twits').html("");
            $('#twits').append(data);
        });
    }, 1000);
*/
}

$(document).ready(nav);
$(document).ready(getSelectedItem);
$(document).ready(getReviewPagination);
$(document).ready(setTwitSubmit);
$(document).ready(getSentimentData);
$(document).ready(getTwitData);
