function chart(){

    var categories = [];
    var positive = [];
    var negative = [];
    var value = [];
    var datas = [];
    $('#chart_category td').each(function(){
	categories.push($(this).html());
    });
    $('#chart_positive td').each(function(){
	positive.push(parseInt($(this).html()));
    });
    $('#chart_negative td').each(function(){
	negative.push(parseInt($(this).html()));
    });
    $('#chart_value td').each(function(){
	value.push($(this).html());
    });
    for(var i = 0 ; i < $('#chart_category td').length ; i++){
	datas.push({
	    name : $($('#chart_category td').get(i)).html(),
	    y : parseInt($($('#chart_total td').get(i)).html()),
	    value : parseInt($($('#chart_value td').get(i)).html())
	});
    }
    $(function () {
	$('#main_chart').highcharts({
            credits: {
		enabled: false
            },
	    exporting: {
		enabled : false
	    },
            chart: {
		type: 'column'
            },
            title: {
		text: $('#product_title h3').html()
            },
	    legend: {
		enabled : false
	    },
            xAxis: {
		categories: categories,
		labels: {
		    staggerLines: 1,
		    style:{
			fontSize : '9px'
		    }
		}
            },
            yAxis: {
		min: 0,
		title: {
		    text: 'Review'
		}
            },
            tooltip: {
		headerFormat: '<span style="font-size:12px">{point.key}</span><br><table style="margin-top:5px;">',
		pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name} : </td>' +
		    '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
		footerFormat: '</table>',
		shared: true,
		useHTML: true
            },
            plotOptions: {
		column: {
		    pointPadding: 0.2,
		    borderWidth: 0
		}
            },
            series: [{
		type: 'column',
		name: '긍정적',
		data: positive,
		color : '#66ccff'
            }, {
		type: 'column',
		name: '부정적',
		data: negative,
		color : '#ff5566'
            }]
	});
    });
    
    $(function () {
	$('#bar_chart').highcharts({
            credits: {
		enabled: false
            },
	    exporting: {
		enabled : false
	    },
            chart: {
		type: 'column'
            },
            title: {
		text: $('#product_title h3').html()
            },
            xAxis: {
		categories: categories
            },
            yAxis: {
		min: 0,
		title: {
		    text: 'Review'
		}
            },
            tooltip: {
		headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name} : </td>' +
                    '<td style="padding:0"><b>{point.y:f}</b></td></tr>',
		footerFormat: '</table>',
		shared: true,
		useHTML: true
            },
            plotOptions: {
		column: {
		    pointPadding: 0.2,
		    borderWidth: 0
		}
            },
            series: [{
		type: 'column',
		name: '긍정적',
		data: positive,
		color : '#66ccff'
            }, {
		type: 'column',
		name: '부정적',
		data: negative,
		color : '#ff5566'
            }]
	});
    });
    $(function () {
	$('#pi_chart').highcharts({
            credits: {
		enabled: false
            },
	    exporting: {
		enabled : false
	    },
            chart: {
		plotBackgroundColor: null,
		plotBorderWidth: null,
		plotShadow: false,
            },
            title: {
		text: ""
            },
            tooltip: {
    		pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
		pie: {
		    allowPointSelect: true,
		    cursor: 'pointer',
		    dataLabels: {
			enabled: true,
			format: '<b>{point.name}</b>: {point.percentage:.1f} %',
			style: {
			    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
			}
		    }
		}
            }, 
            series: [{
		type: 'pie',
		name: '언급 비율',
		data: datas,
		dataLabels: {
		    enabled: true
		},
		center: [170, 120],
		size: 150,
		point: {
		    events: {
			click: function(event){
			    chartLoad(this.value);
			}
		    }
		}
            }]
	});
    });
};

$(document).ready(chart);