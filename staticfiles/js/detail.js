var sentiment_type = 0;
var product_number;

function initNavEvent(){
    $('#nav1').click(function(){
	$('html,body').animate({
            scrollTop: $("#product").offset().top - 20},'slow');
    });
    $('#nav2').click(function(){
	$('html,body').animate({
            scrollTop: $("#review").offset().top - 20},'slow');
    });
    $('#nav3').click(function(){
	$('html,body').animate({
            scrollTop: $("#twit").offset().top - 20},'slow');
    });
}

function initScrollEvent(){
    var state = 0;
    $('#header-left').css('position', 'fixed');
    $('#header-content').css('float', 'right');
    $(window).scroll(function(){
	if($(window).scrollTop() < 100 && state == 1){
	    $('#header-left').animate({
		marginTop : 0,
		paddingLeft : 10
	    });
	    state = 0;
	}
	else if($(window).scrollTop() > 100 && state == 0){
	    $('#header-left').animate({
		marginTop : 20,
		width : 220,
		padding : 0
	    });
	    state = 1;
	}
    });
}

function highlightMarker(str, type){
    var highlightElemList = [];
    var complete_sentence = "";
    if(typeof type  !== "undefined"){
        var attribute_class = (type == 0) ? "attribute_good" : "attribute_bad";
        var sentiment_class = (type == 0) ? "sentiment_good" : "sentiment_bad";
    }
    else{
        var attribute_class = (sentiment_type == 0) ? "attribute_good" : "attribute_bad";
        var sentiment_class = (sentiment_type == 0) ? "sentiment_good" : "sentiment_bad";
    }
    for(var e in str){
        var sentence = str[e]['sentence'];
        var highlight = str[e]['highlighting'];

        if(highlight['sentiment'])
            highlight['sentiment'].forEach(function(elem){highlightElemList.push(['sentiment', elem]);});
        if(highlight['attr'])
            highlight['attr'].forEach(function(elem){highlightElemList.push(['attr', elem]);});

        highlightElemList.sort(function(a, b){
            return (a[1][0] < b[1][0]) ? -1 : 1;
        });
        highlightElemList.forEach(function(elem, index, arr){
            var sub_sentence = "";
            if(elem[0] == "sentiment")
                sub_sentence = "<font class=\"" + sentiment_class + "\">" + sentence.substring(elem[1][0], Number(elem[1][1])+1) + "</font>";
            else
                sub_sentence = "<font class=\"" + attribute_class + "\">" + sentence.substring(elem[1][0], Number(elem[1][1])+1) + "</font>";

            if(index != 0){
                var sub_sentence2 = sentence.substring(arr[index-1][1][1]+1, arr[index][1][0]);
                complete_sentence = complete_sentence + sub_sentence2 + sub_sentence;
            }
            else if(index == 0){
                var sub_sentence3 = sentence.substring(0, arr[index][1][0]);
                complete_sentence = complete_sentence + sub_sentence3  +sub_sentence;
            }
            if(index == arr.length-1){
                complete_sentence = complete_sentence + sentence.substring(arr[index][1][1]+1, sentence.length);
            }
        });
        highlightElemList = [];
        complete_sentence += "<br>";
        if(complete_sentence.length > 500)break;
    }

    return complete_sentence;
}

function getHighlight(str){
    var sentences = str['sentences'];
    var complete_sentence = "";
    
    if(sentiment_type == 3){
        if(sentences['1'] != "undefined")
            complete_sentence += highlightMarker(sentences['1'], 1);
        if(sentences['0'] != "undefined")
            complete_sentence += highlightMarker(sentences['0'], 0);
    }
    if(sentiment_type != 3){
        complete_sentence += highlightMarker(sentences);
    }
    
    return complete_sentence;
}

function setReviewData(jsonObj){
    var contents = jsonObj['data']['info'];
    var page = jsonObj['data']['page'];
    var maxpage = jsonObj['data']['maxpage'];
    $('#review_content').html("");
    $('#current_page').html("");
    $('#max_page').html("");
    for(var e in contents){
        var substringTitle = ((new String(contents[e]['title'])).length > 50) ? (new String(contents[e]['title'])).substring(0, 50)+"...." : contents[e]['title'];
	var pic = "";
	if(contents[e]['images'][0] != undefined){
	    pic = "<div class=\"review_pic\"><img src=\"/static/img/" + contents[e]['images'][0] + "\"></img></div>"
	}
        var el = $("<div class=\"review\">"
		   + pic
		   + "<div class=\"product_review_detail\">"
		   + "<p class=\"review_title\" title=\"" + contents[e]['title'] + "\"><a href=\"" + contents[e]['url'] + "\" target=\"_blank\">" + substringTitle + "</a></p>"
		   + "<p class=\"review_date\">" + contents[e]['creation_time'] + "</p>"
		   + "<p class=\"review_content\">" + getHighlight(contents[e]) + "</p></div></div>");
        $('#review_content').append(el);
    }
    $('#current_page').append(page);
    $('#max_page').append(maxpage);
    if(page == 1){
	$('.paging').each(function(){
	    $(this).html((parseInt(this.text) - 1) % 5 + 1);
	    if(parseInt(this.text) > parseInt($('#max_page').html())){
		$(this).hide();
	    }
	    else{
		$(this).show();
	    }
	});
    }
    $('.paging').each(function(){
	if(parseInt(this.text) == page){
	    $(this).hover(function(){
		$(this).css('color', '#ffffff');
		$(this).css('background-color', '#7abd30');
	    });
	    $(this).css('color', '#ffffff');
	    $(this).css('background-color', '#7abd30');
	}
	else{
	    $(this).hover(function(){
		$(this).css('color', '#ffffff');
		$(this).css('background-color', '#7abd30');
	    }, function(){
		$(this).css('color', '#7e7e7e');
		$(this).css('background-color', '#ffffff');
	    });
	    $(this).css('color', '#7e7e7e');
	    $(this).css('background-color', '#ffffff');
	}
    });
    $('#prev_page').attr('value', Number(jsonObj['data']['page'])-1);
    $('#next_page').attr('value', Number(jsonObj['data']['page'])+1);
}

function getSelectedItem(){
    $("input[name='filter_item']").click(function(){
        $('#nav2').click();
        $('#procon_tab').show();
        getReviewData(1);
        getTwitData($("input[name='filter_item']:checked").val());
    });
}

function getSentimentData(){
    $('#pros_button').click(function(){
        setSentimentButtonState();
        sentiment_type = 0;
	    $('#pros_button').css('color', '#7abd30');
	    $('#pros_button').css('border-bottom', '3px solid #7abd30');
        getReviewData(1);
    });
    $('#cons_button').click(function(){
        setSentimentButtonState();
        sentiment_type = 1;
	    $('#cons_button').css('color', '#7abd30');
	    $('#cons_button').css('border-bottom', '3px solid #7abd30');
        getReviewData(1);
    });
    $('#all_button').click(function(){
        setSentimentButtonState();
        sentiment_type=3;
        $('#all_button').css('color', '#7abd30');
        $('#all_button').css('border-bottom', '3px solid #7abd30');
        getReviewData(1);
    });
}

function setSentimentButtonState(){
    if(sentiment_type == 0){
        $('#pros_button').css('color', '#c2c2c2');
        $('#pros_button').css('border-bottom', '3px solid #ffffff');
    }
    else if(sentiment_type == 1){
        $('#cons_button').css('color', '#c2c2c2');
        $('#cons_button').css('border-bottom', '3px solid #ffffff');
    }
    else if(sentiment_type == 3){
        $('#all_button').css('color', '#c2c2c2');
        $('#all_button').css('border-bottom', '3px solid #ffffff');
    }
}

function getReviewPagination(){
    $('#prev_page').click(function(){
        var page = $('#prev_page').attr('value');
	var page_group = parseInt(parseInt(page)/5) - 1;
        if(Number(page_group + 1) >= 1){
            getReviewData((((page_group) * 5) + 1));
	    $('.paging').each(function(){
		$(this).html(parseInt(this.text) - 5);
		$(this).show();
	    });
        }
    });

    $('#next_page').click(function(){
        var page = $('#prev_page').attr('value');
	var page_group = parseInt(parseInt(page)/5) + 1;
        if(Number(((page_group - 1) * 5) + 5) < Number($('#max_page').html())){
            getReviewData(((page_group * 5) + 1));
	    $('.paging').each(function(){
		$(this).html(parseInt(this.text) + 5);
		if(parseInt(this.text) > parseInt($('#max_page').html())){
		    $(this).hide();
		}
	    });
        }
    });
    $('.paging').click(function(){
	getReviewData("" + this.text);
    });
    $('#max_page').click(function(){
	getReviewData("" + this.text);
	var page_group = parseInt((parseInt(this.text)-1) / 5);
	$('.paging').each(function(){
	    var paging_index = (parseInt(this.text) - 1) % 5 + 1;
	    var page_num = page_group * 5;
	    $(this).html(paging_index + page_num);
	    if(parseInt(this.text) > parseInt($('#max_page').html())){
		$(this).hide();
	    }
	});
    });
}

function getReviewData(page, order){
    var item_val = $("input[name='filter_item']:checked").val();
    if(item_val == 11) item_val = undefined;
    var get_data = $.get('http://reviewgem.co.kr/review_list', {filter_value : item_val, page : page, product_id : product_number, sentiment_type : sentiment_type , order : order});
    get_data.done(function(data){
        setReviewData(data);
    });
}

function setTwitSubmit(){
    var input_data = $('#twit_input').val();
    $('#twit_submit').click(function(){
//        var twit_submit = $.get('http://reviewgem.co.kr/twit_submit', {product_id : product_number, twit_input : $('#twit_input').val()});
	    var tag_top = $('#twit_form').offset().top - 100;
	    var tag_left = $('#twit_form').offset().left;
	    $('#twit_tag').css('top',tag_top);
	    $('#twit_tag').css('left',tag_left);
	    $('#twit_tag').show();
    });
    $('#tag_submit').click(function(){
        var twit_tag = $("input[name='twit_tag']:checked").val();
        var twit_submit = $.get('http://reviewgem.co.kr/twit_submit', {product_id : product_number, twit_input : $('#twit_input').val(), twit_tag : twit_tag, name : $('#twit_name').val(), password : $('#twit_password').val()})
            .done(function(data){
                getTwitData($("input[name='filter_item']:checked").val());
                $('#twit_input').val("");
                $('#twit_tag').hide();
            });
    });
}

function setTagSubmit(){
    $('#tag_close').click(function(){
	    $('#twit_tag').hide();
    });
}

function getTwitScroll(){
    $('#twits').scroll(function(){
        var elem = $('#twits');
        if( elem[0].scrollHeight - elem.scrollTop() + 7 == elem.outerHeight() ) {
            var twit_data = $.get('http://reviewgem.co.kr/twit_list', {product_id : product_number });
            twit_data.done(function(data){
                $('#twits').html("");
                $('#twits').append(data);
            });
        }
    });
}

function likeButton(event){
    var elem = $(event.target);
    if(elem.attr('class') == "twit_like_btn"){
        var likeBtn = $.get('http://reviewgem.co.kr/like', {twit_id : elem.attr('value')})
            .done(function(){
                getTwitData($("input[name='filter_item']:checked").val());
            });
    }
    else if(elem.attr('class') == "pro_btn"){
        var likeBtn = $.get('http://reviewgem.co.kr/like', {pro_id : elem.attr('value')})
            .done(function(){
            });
    }
    else if(elem.attr('class') == "con_btn"){
    }
}

function updateTwitData(event){ 
    var elem = $(event.target);
    if(elem.attr('id') == "twit_delete"){
	    var tag_top = $('#twit_form').offset().top - 50;
        var tag_left = $('#twit_form').offset().left;
        $('#twit_update_submit').attr('value', elem.val());
    	$('#twit_delete_form').css('top',tag_top);
        $('#twit_delete_form').css('left',tag_left);
        $('#twit_delete_form').show();
    }else if(elem.attr('id') == "twit_edit"){
	    var tag_top = $('#twit_form').offset().top - 50;
        var tag_left = $('#twit_form').offset().left;
        $('#twit_edit_submit').attr('value', elem.val());
    	$('#twit_edit_form').css('top',tag_top);
        $('#twit_edit_form').css('left',tag_left);
        $('#twit_edit_form').show();
    }
}

function deleteTwitSubmit(){
    $('#twit_update_submit').click(function(){
        $.get("http://reviewgem.co.kr/twit_update", { twit_id : $('#twit_update_submit').val(), password : $('#del_password').val(), update_type : "delete" })
            .done(function(){
                getTwitData($("input[name='filter_item']:checked").val());
                $('#twit_delete_form').hide();
            }).fail(function(){
                alert("비밀번호를 확인하세요");
            });
    });
}

function editTwitSubmit(){
    $('#twit_edit_submit').click(function(){
        $.get("http://reviewgem.co.kr/twit_update", { twit_id :  $('#twit_edit_submit').val(), password : $('#edit_password').val(), content : $('#edit_content').val() ,update_type : "edit"})
            .done(function(){
                getTwitData($("input[name='filter_item']:checked").val());
                $('#twit_edit_form').hide();
            }).fail(function(){
                alert("비밀번호를 확인하세요");
            });
    });
}

function getTwitData(twit_filter_value){
    var twit_data = $.get('http://reviewgem.co.kr/twit_list', {product_id : product_number, twit_filter : twit_filter_value});
    twit_data.done(function(data){
        $('#twits').html("");
        $('#twits').append(data);
    });
}

function chartLoad(value){
    $("input[name='filter_item']").each(function(){
	if(this.value == value){
	    $(this).click();
	}
    });
}

function twitKeyCount(){
    $('#twit_input').keydown(function(){
        var text = $('#twit_input').val();
        if(text.length < 50){
            $('#input_string').text("");
            $('#input_string').text(Number(text.length)+1);
        }
    });
}

function getReviewOrder() {
    $('#date_order').click(function(){
        getReviewData(1, "date");
    });
}

window.onload = function(){
    product_number = $('#product_number').val();
    $('#product_like_img').attr('value', product_number);
    $('#product_branch').attr('href', '/detail/' + product_number);
    $('#procon_tab').hide();
    var get_data = $.get('http://reviewgem.co.kr/review_list', {page : 1, product_id : product_number});
    get_data.done(function(data){
        setReviewData(data);
    });

    $('#procon_tab').show();
    $('#pros_button').click();
    getTwitData(11);
/*
    setInterval(function(){
        var twit_data = $.get('http://reviewgem.co.kr/twit_list', {product_id : product_number});
        twit_data.done(function(data){
            $('#twits').html("");
            $('#twits').append(data);
        });
    }, 1000);
*/
}

$(document).ready(twitKeyCount);
$(document).ready(initNavEvent);
$(document).ready(initScrollEvent);
$(document).ready(getSelectedItem);
$(document).ready(getReviewPagination);
$(document).ready(setTwitSubmit);
$(document).ready(setTagSubmit);
$(document).ready(getSentimentData);
$(document).ready(getTwitScroll);
$(document).ready(likeButton);
$(document).ready(getReviewOrder);
$(document).ready(deleteTwitSubmit);
$(document).ready(editTwitSubmit);
