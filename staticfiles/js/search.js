function searchHighlight(){
    
    var input = $('#query').attr('value');
    var title_output = '<font style="color:blue; ">' + input + '</font>';
    var content_output = '<font style="color:blue; ">' + input + '</font>';

    var query =new RegExp($('#query').attr('value'), "g");

    $('.review_content').each(function(){
	var content = $(this).html();
	var new_content = content.replace(query, content_output);
	$(this).html(new_content);
    });

    $('.review_title').each(function(){
	var content = $(this).html();
	var new_content = content.replace(query, title_output);
	$(this).html(new_content);
    });

}
$(document).ready(searchHighlight);

