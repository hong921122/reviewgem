function cateoff(){
    $('#category_phone .tab_title').css('color','#838383');
    $('#category_phone .tab_title').css('border-bottom','3px solid #ffffff');
    $('#category_camera .tab_title').css('color','#838383');
    $('#category_camera .tab_title').css('border-bottom','3px solid #ffffff');
}

function init(){
    $('#phone_detail').css('width', '60%');
    $('#camera_detail').css('width', '0');
    $('#camera_detail').css('opacity', '0');
    $('#camera_detail .detail').css('width', '0');
    $('#camera_detail .detail').css('height', '0');
    $('#category_phone .cate_wrapper').css('border-right', 'none');
    $('#phone_detail .cate_wrapper').css('border-left', 'none');
    $('#category_phone .tab_title').css('color','#7abd30');
    $('#category_phone .tab_title').css('border-bottom','3px solid #7abd30');
}

function category(){
    $('#category_phone').hover(function() {
	if($('#phone_detail').css('width') == '0px'){
	    cateoff();
	    $('#category_phone .cate_wrapper').css('border-right', 'none');
	    $('#phone_detail .cate_wrapper').css('border-left', 'none');
	    $('#category_phone .tab_title').css('color','#7abd30');
	    $('#category_phone .tab_title').css('border-bottom','3px solid #7abd30');

	    $('#phone_detail').animate({
		width : '60%',
		opacity: 1
	    }, 300);
	    $('#phone_detail .detail').animate({
		width : '5px'
	    }, 150).animate({
		height : '220px'
	    },0).animate({
		width : '155px'
	    }, 150);
	    $('#phone_detail .detail a').animate({
		opacity : 1
	    }, 400);

	    $('#camera_detail').animate({
		width : 0,
		opacity: 0
	    }, 300);
	    $('#camera_detail .detail').animate({
		width : 0
	    },150).animate({height : 0}, 0);
	    $('#camera_detail .detail a').animate({
		opacity : 0
	    }, 0);
	}
    });
    $('#category_camera').hover(function() {
	if($('#camera_detail').css('width') == '0px'){
	    cateoff();
	    $('#category_camera .cate_wrapper').css('border-left', 'none');
	    $('#camera_detail .cate_wrapper').css('border-right', 'none');
	    $('#category_camera .tab_title').css('color','#7abd30');
	    $('#category_camera .tab_title').css('border-bottom','3px solid #7abd30');

	    $('#phone_detail').animate({
		width : 0,
		opacity: 0
	    }, 300);
	    $('#phone_detail .detail').animate({
		width : 0
	    }, 150).animate({height : 0}, 0);
	    $('#phone_detail .detail a').animate({
		opacity : 0
	    }, 0);

	    $('#camera_detail').animate({
		width : '60%',
		opacity: 1
	    }, 300);
	    $('#camera_detail .detail').animate({
		width : '5px'
	    },150).animate({
		height : '220px'
	    },0).animate({
		width : '155px'
	    }, 150);
	    $('#camera_detail .detail a').animate({
		opacity : 1
	    }, 400);
	}
    });
}
function scrollEvent(){
    var max_len = $('.detail').length * 150;
    $('#cate_next').mouseenter(function() {
	$(this).find("span").css("background-color","#7abd30");
	$(this).find("span").css("color","#ffffff");
    }).mouseleave(function() {
	$(this).find("span").css("background-color","#f2f2f2");
	$(this).find("span").css("color","#7e7e7e");
    });
    $('#cate_prev').mouseenter(function() {
	$(this).find("span").css("background-color","#7abd30");
	$(this).find("span").css("color","#ffffff");
    }).mouseleave(function() {
	$(this).find("span").css("background-color","#f2f2f2");
	$(this).find("span").css("color","#7e7e7e");
    });

    $('#cate_next').click(function(){
	if(Math.abs(parseInt($('#product_wrapper').css('marginLeft')) - '750') < max_len){
	    $('#product_wrapper').animate({
		marginLeft : '-=750px'
	    },100);
	}
    });
    $('#cate_prev').click(function(){
	if($('#product_wrapper').css('marginLeft') != '100px'){
	    $('#product_wrapper').animate({
		marginLeft : '+=750px'
	    },100);
	}
    });
}
$(document).ready(scrollEvent);
