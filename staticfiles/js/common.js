google.load("webfont", "1");
google.setOnLoadCallback(function(){
    WebFont.load({custom : {
        families : ["NanumGothic"],
        urls : ["http://fontface.kr/NanumGothic/css"]
    }});
});

function errorReport() {
    $('#dialog').dialog({
        autoOpen : false,
        width : 500,
        height : 300
    });

    $('#error_report').click(function(){
        $('#dialog').dialog('open');
    });
}

function errorSubmit() {
    $('#error_submit').click(function(){
        var error_text = $('#error_text').val();
        var error_select = $('#error_select option:selected').text();
        var user_report = $.get('http://reviewgem.co.kr/error_report', {error_text : error_text, error_select : error_select});
        user_report.done(function(data){
            alert("test");
        });
    });
}

$(document).ready(errorReport);
$(document).ready(errorSubmit);
