function cateoff(){
    $('#category_phone').css('color','#838383');
    $('#category_phone').css('border-bottom','3px solid #ffffff');
    $('#category_camera').css('color','#838383');
    $('#category_camera').css('border-bottom','3px solid #ffffff');
}

function category(){
    var cate_code = $('#cate_code').attr('value');
    $('.cate_list li').each(function(){
	if(cate_code == $(this).attr('value')){
	    $(this).children().css('color', '#7abd30');
	}
    });
    $('#category_phone').css('color','#7abd30');
    $('#category_phone').css('border-bottom','3px solid #7abd30');
    $('#category_phone').hover(function() {
	cateoff();
	$('#category_phone').css('color','#7abd30');
	$('#category_phone').css('border-bottom','3px solid #7abd30');
    });
    $('#category_camera').hover(function() {
	cateoff();
	$('#category_camera').css('color','#7abd30');
	$('#category_camera').css('border-bottom','3px solid #7abd30');
    });
}
$(document).ready(category);
