from django.db import models

class Test(models.Model):
    question = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

class Choice(models.Model):
    poll = models.ForeignKey(Test)
    choice = models.CharField(max_length=200)
    votes = models.IntegerField()
